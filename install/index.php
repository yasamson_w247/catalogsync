<?php

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    \Catalog\Sync\Data\Export\ProfileTable as ExportProfileTable,
    \Catalog\Sync\Data\Import\ProfileTable as ImportProfileTable;

Loc::loadLanguageFile(__FILE__);
if (class_exists('catalog_sync'))
    return;

class catalog_sync extends CModule
{
    var $MODULE_ID = 'catalog.sync';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_GROUP_RIGHTS = 'Y';
    protected $agents = [];

    const AGENT_RUNNER_INTERVAL = 60;

    public function __construct()
    {
        $arModuleVersion = array();

        $path = str_replace('\\', '/', __FILE__);
        $path = substr($path, 0, strlen($path) - strlen('/index.php'));
        include($path . '/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('IBLOCKSYNC_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('IBLOCKSYNC_MODULE_DESCRIPTION');

        $this->PARTNER_NAME = Loc::getMessage("IBLOCKSYNC_PARTNER_NAME");
        $this->PARTNER_URI = Loc::getMessage("IBLOCKSYNC_PARTNER_URI");

        $this->agents = [
            [
                '\Catalog\Sync\Agent\Runner::checkProfiles();',
                'catalog.sync',
                'N',
                self::AGENT_RUNNER_INTERVAL,
                ConvertTimeStamp(false, "FULL"),
                'Y',
                ConvertTimeStamp(time() + 120, "FULL"),
            ]
        ];
    }

    public function getModuleDir()
    {
        return getLocalPath("modules/{$this->MODULE_ID}");
    }

    public function doInstall()
    {
        $this->installDB();
        $this->installFiles();
        $this->installAgents();
    }

    public function doUninstall()
    {
        $this->uninstallDB();
        $this->uninstallFiles();
        $this->uninstallAgents();
    }

    function installAgents()
    {
        foreach ($this->agents as $arAgent) {
            CAgent::AddAgent(...$arAgent);
        }
    }

    function uninstallAgents()
    {
        foreach ($this->agents as $arAgent) {
            CAgent::RemoveAgent($arAgent[0], $arAgent[1]);
        }
    }

    function installDB()
    {
        $conn = \Bitrix\Main\Application::getConnection();
        RegisterModule($this->MODULE_ID);
        if (Loader::includeModule($this->MODULE_ID)) {
            if (!$conn->isTableExists(ExportProfileTable::getTableName())) {
                ExportProfileTable::getEntity()->createDbTable();
            }
            if (!$conn->isTableExists(ImportProfileTable::getTableName())) {
                ImportProfileTable::getEntity()->createDbTable();
            }
        }
    }

    function uninstallDB()
    {
        $conn = \Bitrix\Main\Application::getConnection();
        if (Loader::includeModule($this->MODULE_ID)) {
            if ($conn->isTableExists(ExportProfileTable::getTableName())) {
                $conn->dropTable(ExportProfileTable::getTableName());
            }
            if ($conn->isTableExists(ImportProfileTable::getTableName())) {
                $conn->dropTable(ImportProfileTable::getTableName());
            }
        }
        UnRegisterModule($this->MODULE_ID);
    }

    function installFiles()
    {
        if ($_ENV["COMPUTERNAME"] != 'BX') {
            $path = $this->getModuleDir();
            if ($handle = opendir($_SERVER['DOCUMENT_ROOT'] . $path . '/install/admin')) {
                while (false !== ($entry = readdir($handle))) {
                    $filePath = $_SERVER['DOCUMENT_ROOT'] . $path . '/install/admin/' . $entry;
                    $content = file_get_contents($filePath);
                    $content = str_replace('#MODULE_PATH#', $path, $content);
                    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $entry, $content);
                }
            }
            CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "{$path}/install/js", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/js/{$this->MODULE_ID}", true, true);
            CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "{$path}/install/images", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/images/{$this->MODULE_ID}", true, true);
            CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "{$path}/install/components", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components", True, True);
        }
    }

    function uninstallFiles()
    {
        if ($_ENV["COMPUTERNAME"] != 'BX') {
            $path = $this->getModuleDir();
            DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "{$path}/install/admin/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin");
            DeleteDirFilesEx("/bitrix/images/{$this->MODULE_ID}/");//images
            DeleteDirFilesEx("/bitrix/js/{$this->MODULE_ID}/");//javascript
        }
        return true;
    }
}