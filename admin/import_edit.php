<?

use \Bitrix\Main\Loader,
    \Bitrix\Main\Localization\Loc,
    Catalog\Sync\Data\Import\ProfileTable as ImportProfileTable,
    \Bitrix\Main\SystemException;

// подключим все необходимые файлы:
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/subscribe/include.php"); // инициализация модуля
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/subscribe/prolog.php"); // пролог модуля

// подключим языковой файл
IncludeModuleLangFile(__FILE__);
IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/iblock/admin/iblock_xml_import.php');

$MODULE_ID = 'catalog.sync';

Loader::includeModule($MODULE_ID);

CJSCore::Init(['jquery']);

// получим права доступа текущего пользователя на модуль
$POST_RIGHT = $APPLICATION->GetGroupRight("catalog.sync");
$app = \Bitrix\Main\Application::getInstance();
$context = $app->getContext();
$request = $context->getRequest();
$documentRoot = \Bitrix\Main\Application::getDocumentRoot();
$modulePath = getLocalPath("modules/catalog.sync");

// если нет прав - отправим к форме авторизации с сообщением об ошибке
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

// сформируем список закладок
$aTabs = array(
    array("DIV" => "edit1", "TAB" => GetMessage("rub_tab_rubric"), "ICON" => "main_user_edit", "TITLE" => GetMessage("rub_tab_rubric_title")),
    array("DIV" => "edit2", "TAB" => GetMessage("rub_tab_generation"), "ICON" => "main_user_edit", "TITLE" => GetMessage("rub_tab_generation_title")),
    array("DIV" => "edit3", "TAB" => GetMessage("rub_tab_schedules"), "ICON" => "main_user_edit", "TITLE" => GetMessage("rub_tab_schedules_title")),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$ID = intval($ID);        // идентификатор редактируемой записи
$message = null;        // сообщение об ошибке
$bVarsFromForm = false; // флаг "Данные получены с формы", обозначающий, что выводимые данные получены с формы, а не из БД.

// значения по умолчанию
$str_SORT = 100;
$str_ACTIVE = "Y";
$str_LAST_EXECUTED = ConvertTimeStamp(false, "FULL");
$str_FROM_FIELD = COption::GetOptionString($MODULE_ID, "default_from");

// выборка данных
if ($ID > 0) {
    $profile = ImportProfileTable::getByPrimary($ID);
    $arFields = $profile->fetch();
    if (!$arFields || !extract($arFields, EXTR_PREFIX_ALL, "str"))
        $ID = 0;
}

$defaultSettings = [
    'outFileAction' => 'N',
    'GENERATE_PREVIEW' => 'Y',
    'USE_CRC' => 'Y',
];

$str_SETTINGS = array_merge($defaultSettings, $str_SETTINGS ?: []);

// ******************************************************************** //
//                ОБРАБОТКА ИЗМЕНЕНИЙ ФОРМЫ                             //
// ******************************************************************** //


if ($_REQUEST['action'] == 'run' && $_REQUEST['ID']) {
    $psList = \Catalog\Sync\System::ps();
    $psList = array_filter($psList, function ($v, $k) use ($documentRoot, $modulePath, $APPLICATION) {
        if (strpos($v['cmd'], "php") !== false && (
                strpos($v['cmd'], "-f {$documentRoot}{$modulePath}/lib/cli/import.php") !== false
            )) {
            if (strpos($v['cmd'], "--id={$_REQUEST['ID']}") !== false) {
                $APPLICATION->ThrowException(Loc::getMessage("PROFILE_PROCESS_EXISTS"));
                return true;
            }
        }
        return false;
    }, ARRAY_FILTER_USE_BOTH);
    if (!$psList) {
        \Catalog\Sync\Import::run($_REQUEST['ID']);
    }
    LocalRedirect($APPLICATION->GetCurPageParam("", ['action']));
}

if (
    $REQUEST_METHOD == "POST" // проверка метода вызова страницы
    &&
    ($save != "" || $apply != "") // проверка нажатия кнопок "Сохранить" и "Применить"
    &&
    $POST_RIGHT == "W"          // проверка наличия прав на запись для модуля
    &&
    check_bitrix_sessid()     // проверка идентификатора сессии
) {

    $SETTINGS = array_merge($str_SETTINGS ?: [], $SETTINGS);
    // обработка данных формы
    $arFields = Array(
        "ACTIVE" => $ACTIVE == "Y",
        "NAME" => $NAME,
        //"LAST_EXECUTED" => $LAST_EXECUTED
        "SETTINGS" => $SETTINGS ?: array()
    );

    // сохранение данных
    if ($ID > 0) {
        $res = ImportProfileTable::update($ID, $arFields);
    } else {
        $res = ImportProfileTable::add($arFields);
        $ID = $res->getId();
    }

    if ($res->isSuccess()) {
        // если сохранение прошло удачно - перенаправим на новую страницу
        // (в целях защиты от повторной отправки формы нажатием кнопки "Обновить" в браузере)
        if ($apply != "")
            // если была нажата кнопка "Применить" - отправляем обратно на форму.
            LocalRedirect("/bitrix/admin/catalog_sync_import_edit.php?ID=" . $ID . "&mess=ok&lang=" . LANG . "&" . $tabControl->ActiveTabParam());
        else
            // если была нажата кнопка "Сохранить" - отправляем к списку элементов.
            LocalRedirect("/bitrix/admin/catalog_sync_import_list.php?lang=" . LANG);
    } else {
        // если в процессе сохранения возникли ошибки - получаем текст ошибки и меняем вышеопределённые переменные
        if ($e = implode('<br>', $res->getErrorMessages())) {
            $message = new CAdminMessage(
                [
                    'MESSAGE' => GetMessage("rub_save_error"),
                    'HTML' => true,
                ],
                new CApplicationException($e, $id)
            );
        }
        $bVarsFromForm = true;
    }
}

// ******************************************************************** //
//                ВЫБОРКА И ПОДГОТОВКА ДАННЫХ ФОРМЫ                     //
// ******************************************************************** //

// дополнительная подготовка данных
if ($ID > 0 && !$message)
    $DAYS_OF_WEEK = explode(",", $str_DAYS_OF_WEEK);
if (!is_array($DAYS_OF_WEEK))
    $DAYS_OF_WEEK = array();

// если данные переданы из формы, инициализируем их
if ($bVarsFromForm)
    $DB->InitTableVarsForEdit(ImportProfileTable::getTableName(), "", "str_");

// ******************************************************************** //
//                ВЫВОД ФОРМЫ                                           //
// ******************************************************************** //

// установим заголовок страницы
$APPLICATION->SetTitle(($ID > 0 ? GetMessage("rub_title_edit") . $ID : GetMessage("rub_title_add")));

// не забудем разделить подготовку данных и вывод
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

// конфигурация административного меню
$aMenu = array(
    array(
        "TEXT" => GetMessage("rub_list"),
        "TITLE" => GetMessage("rub_list_title"),
        "LINK" => "catalog_sync_import_list.php?lang=" . LANG,
        "ICON" => "btn_list",
    )
);

if ($ID > 0) {
    $aMenu[] = array("SEPARATOR" => "Y");
    $aMenu[] = array(
        "TEXT" => GetMessage("rub_add"),
        "TITLE" => GetMessage("rubric_mnu_add"),
        "LINK" => "catalog_sync_import_edit.php?lang=" . LANG,
        "ICON" => "btn_new",
    );
    $aMenu[] = array(
        "TEXT" => GetMessage("rub_delete"),
        "TITLE" => GetMessage("rubric_mnu_del"),
        "LINK" => "javascript:if(confirm('" . GetMessage("rubric_mnu_del_conf") . "'))window.location='catalog_sync_import_list.php?ID=" . $ID . "&action=delete&lang=" . LANG . "&" . bitrix_sessid_get() . "';",
        "ICON" => "btn_delete",
    );
    if ($_REQUEST['ID']) {
        $runUrl = $APPLICATION->GetCurPageParam("action=run", ['action']);
        $aMenu[] = array(
            "TEXT" => GetMessage("rub_run"),
            "TITLE" => GetMessage("rubric_mnu_run"),
            "LINK" => "javascript:if(confirm('" . GetMessage("rubric_mnu_run_conf") . "'))window.location='{$runUrl}';",
            "ICON" => "",
        );
    }
}

// создание экземпляра класса административного меню
$context = new CAdminContextMenu($aMenu);

// вывод административного меню
$context->Show();
?>

<?
// если есть сообщения об ошибках или об успешном сохранении - выведем их.
if ($_REQUEST["mess"] == "ok" && $ID > 0)
    CAdminMessage::ShowMessage(array("MESSAGE" => GetMessage("rub_saved"), "TYPE" => "OK"));

if ($message)
    echo $message->Show();
elseif ($rubric->LAST_ERROR != "")
    CAdminMessage::ShowMessage($rubric->LAST_ERROR);
?>

<?
/*$progressMessage = new CAdminMessage([
    'MESSAGE' => '123',
    'TYPE' => 'PROGRESS',
    'HTML' => true,
    'DETAILS' => '#PROGRESS_BAR#',
    'PROGRESS_ICON' => false,
    'PROGRESS_WIDTH' => 500,
    'PROGRESS_TOTAL' => 100,
    'PROGRESS_VALUE' => 0,
    'PROGRESS_TEMPLATE' => GetMessage('PROFILE_PROGRESS_TEMPLATE'),
]);
echo $progressMessage->Show();*/
?>

<?
// далее выводим собственно форму
?>
<form method="POST" Action="<? echo $APPLICATION->GetCurPage() ?>" ENCTYPE="multipart/form-data" name="post_form">
    <? // проверка идентификатора сессии ?>
    <? echo bitrix_sessid_post(); ?>
    <?
    // отобразим заголовки закладок
    $tabControl->Begin();
    ?>
    <?
    //********************
    // первая закладка - форма редактирования параметров рассылки
    //********************
    $tabControl->BeginNextTab();
    ?>
    <tr>
        <td width="40%"><? echo GetMessage("rub_act") ?>:</td>
        <td width="60%"><input type="checkbox" name="ACTIVE" value="Y"<? if ($str_ACTIVE == 1) echo " checked" ?>>
        </td>
    </tr>
    <tr>
        <td><span class="required">*</span><? echo GetMessage("rub_name") ?>:</td>
        <td><input type="text" name="NAME" value="<? echo $str_NAME; ?>" size="30" maxlength="100"></td>
    </tr>
    <tr>
        <td><? echo GetMessage("PROFILE_STATUS") ?>:</td>
        <td>
            <strong id="prof_<?= $_REQUEST['ID'] ?>_status"><?= GetMessage("PROFILE_STATUS_" . $str_SETTINGS['STATUS']) ?></strong>
        </td>
    </tr>
    <tr id="profile_error" class="hidden">
        <td><? echo GetMessage("PROFILE_ERROR") ?>:</td>
        <td>
            <strong id="prof_<?= $_REQUEST['ID'] ?>_errors_msg"></strong>
        </td>
    </tr>
    <tr>
        <td><? echo GetMessage("PROFILE_LAST_EXEC") ?>:</td>
        <td>
            <strong id="prof_<?= $_REQUEST['ID'] ?>_last_exec"><?= $str_SETTINGS['LAST_EXEC'] ? date("d.m.Y, H:i:s", $str_SETTINGS['LAST_EXEC']) : "" ?></strong>
        </td>
    </tr>
    <tr>
        <td><? echo GetMessage("PROFILE_FILE_URL") ?>:</td>
        <td>
            <? if ($str_SETTINGS['URL_DATA_FILE']): ?>
                <strong><a href="<?= $str_SETTINGS['URL_DATA_FILE'] ?>"
                           target="_blank"><?= $str_SETTINGS['URL_DATA_FILE'] ?></a></strong>
            <? endif; ?>
        </td>
    </tr>
    <?
    $resultKeys = [
        'ELEMENTS_UPD',
        'ELEMENTS_ADD',
        'ELEMENTS_ERR',
        'ELEMENTS_DEL',
        'ELEMENTS_DEA',
    ];
    ?>
    <? foreach ($resultKeys as $key): ?>
        <tr>
            <td><? echo GetMessage("PROFILE_{$key}") ?>:</td>
            <td>
                <strong id="prof_<?= $_REQUEST['ID'] ?>_<?= mb_strtolower($key) ?>"><?= $str_SETTINGS[$key] ?></strong>
            </td>
        </tr>
    <? endforeach; ?>
    <?
    //********************
    // вторая закладка - параметры автоматической генерации рассылки
    //********************
    $tabControl->BeginNextTab();
    ?>
    <tr class="adm-detail-required-field">
        <td width="40%"><? echo GetMessage("IBLOCK_CML2_URL_DATA_FILE") ?>:</td>
        <td width="60%">
            <input type="text" id="URL_DATA_FILE" name="SETTINGS[URL_DATA_FILE]" size="30"
                   value="<?= htmlspecialcharsbx($str_SETTINGS['URL_DATA_FILE']) ?>">
            <input type="button" value="<? echo GetMessage("IBLOCK_CML2_OPEN") ?>" OnClick="BtnClick()">
            <?
            CAdminFileDialog::ShowScript
            (
                Array(
                    "event" => "BtnClick",
                    //"arResultDest" => array("FORM_NAME" => "form1", "FORM_ELEMENT_NAME" => "URL_DATA_FILE"),
                    "arResultDest" => array("ELEMENT_ID" => "URL_DATA_FILE"),
                    "arPath" => array("SITE" => SITE_ID, "PATH" => "/upload"),
                    "select" => 'F',// F - file only, D - folder only
                    "operation" => 'O',
                    "showUploadTab" => true,
                    "showAddToMenuTab" => false,
                    "fileFilter" => 'xml',
                    "allowAllFiles" => true,
                    "SaveConfig" => true,
                )
            );
            ?>
        </td>
    </tr>
    <tr class="adm-detail-required-field">
        <td><? echo GetMessage("IBLOCK_CML2_IBLOCK_TYPE") ?>:</td>
        <td>
            <? echo GetIBlockDropDownListEx(
                $str_SETTINGS['IBLOCK_ID'],
                'SETTINGS[IBLOCK_TYPE]',
                'SETTINGS[IBLOCK_ID]',
                array(
                    "MIN_PERMISSION" => "X",
                    "OPERATION" => "iblock_export",
                ),
                '',
                '',
                'class="adm-detail-iblock-types"',
                'class="adm-detail-iblock-list"'
            ); ?>
        </td>
    </tr>
    <tr class="adm-detail-required-field">
        <td class="adm-detail-valign-top"><? echo GetMessage("IBLOCK_CML2_LID") ?></td>
        <td><? echo CLang::SelectBoxMulti("SETTINGS[LID]", $str_SETTINGS['LID']); ?></td>
    </tr>
    <tr>
        <td class="adm-detail-valign-top"><? echo GetMessage("IBLOCK_CML2_ACTION") ?>:</td>
        <td>
            <input type="radio" name="SETTINGS[outFileAction]" value="N" id="outFileAction_N"
                   <? if ($str_SETTINGS['outFileAction'] == 'N'): ?>checked="checked"<? endif; ?>><label
                    for="outFileAction_N"><? echo GetMessage("IBLOCK_CML2_ACTION_NONE") ?></label><br>
            <input type="radio" name="SETTINGS[outFileAction]" value="A" id="outFileAction_A"
                   <? if ($str_SETTINGS['outFileAction'] == 'A'): ?>checked="checked"<? endif; ?>><label
                    for="outFileAction_A"><? echo GetMessage("IBLOCK_CML2_ACTION_DEACTIVATE") ?></label><br>
            <input type="radio" name="SETTINGS[outFileAction]" value="D" id="outFileAction_D"
                   <? if ($str_SETTINGS['outFileAction'] == 'D'): ?>checked="checked"<? endif; ?>><label
                    for="outFileAction_D"><? echo GetMessage("IBLOCK_CML2_ACTION_DELETE") ?></label><br>
        </td>
    </tr>
    <tr>
        <td><? echo GetMessage("IBLOCK_CML2_IMAGE_RESIZE") ?>:</td>
        <td>
            <input type="checkbox" id="GENERATE_PREVIEW" name="SETTINGS[GENERATE_PREVIEW]" value="Y"
                   <? if ($str_SETTINGS['GENERATE_PREVIEW'] == 'Y'): ?>checked<? endif; ?>>
        </td>
    </tr>
    <tr>
        <td><? echo GetMessage("IBLOCK_CML2_USE_CRC") ?>:</td>
        <td>
            <input type="checkbox" id="USE_CRC" name="SETTINGS[USE_CRC]" value="Y"
                   <? if ($str_SETTINGS['USE_CRC'] == 'Y'): ?>checked<? endif; ?>>
        </td>
    </tr>

    <?
    $tabControl->BeginNextTab();
    ?>
    <tr>
        <td><label for="SCHEDULE_USE"><? echo GetMessage("SCHEDULE_USE") ?>:</label></td>
        <td>
            <input name="SETTINGS[SCHEDULE][USE]" id="SCHEDULE_USE" type="checkbox"
                   value="Y" <?= $str_SETTINGS['SCHEDULE']['USE'] == 'Y' ? 'checked="checked"' : '' ?>>
        </td>
    </tr>
    <tr>
        <td><label for="SCHEDULE_MINUTES"><? echo GetMessage("SCHEDULE_MINUTES") ?>:</label></td>
        <td>
            <input name="SETTINGS[SCHEDULE][MINUTES]" id="SCHEDULE_MINUTES" type="text"
                   value="<?= $str_SETTINGS['SCHEDULE']['MINUTES'] ?>">
        </td>
    </tr>
    <tr>
        <td><label for="SCHEDULE_HOURS"><? echo GetMessage("SCHEDULE_HOURS") ?>:</label></td>
        <td>
            <input name="SETTINGS[SCHEDULE][HOURS]" id="SCHEDULE_HOURS" type="text"
                   value="<?= $str_SETTINGS['SCHEDULE']['HOURS'] ?>">
        </td>
    </tr>
    <tr>
        <td><label for="SCHEDULE_DAYS"><? echo GetMessage("SCHEDULE_DAYS") ?>:</label></td>
        <td>
            <input name="SETTINGS[SCHEDULE][DAYS]" id="SCHEDULE_DAYS" type="text"
                   value="<?= $str_SETTINGS['SCHEDULE']['DAYS'] ?>">
        </td>
    </tr>
    <tr>
        <td><label for="SCHEDULE_MONTHS"><? echo GetMessage("SCHEDULE_MONTHS") ?>:</label></td>
        <td>
            <input name="SETTINGS[SCHEDULE][MONTHS]" id="SCHEDULE_MONTHS" type="text"
                   value="<?= $str_SETTINGS['SCHEDULE']['MONTHS'] ?>">
        </td>
    </tr>
    <tr>
        <td><label for="SCHEDULE_DAYS_OF_WEEK"><? echo GetMessage("SCHEDULE_DAYS_OF_WEEK") ?>:</label></td>
        <td>
            <input name="SETTINGS[SCHEDULE][DAYS_OF_WEEK]" id="SCHEDULE_DAYS_OF_WEEK" type="text"
                   value="<?= $str_SETTINGS['SCHEDULE']['DAYS_OF_WEEK'] ?>">
        </td>
    </tr>
    <?
    // завершение формы - вывод кнопок сохранения изменений
    $tabControl->Buttons(
        array(
            "disabled" => ($POST_RIGHT < "W"),
            "back_url" => "rubric_admin.php?lang=" . LANG,

        )
    );
    ?>
    <input type="hidden" name="lang" value="<?= LANG ?>">
    <? if ($ID > 0 && !$bCopy): ?>
        <input type="hidden" name="ID" value="<?= $ID ?>">
    <? endif; ?>
    <?
    // завершаем интерфейс закладок
    $tabControl->End();
    ?>

    <?
    // дополнительное уведомление об ошибках - вывод иконки около поля, в котором возникла ошибка
    $tabControl->ShowWarnings("post_form", $message);
    ?>

    <?
    // дополнительно: динамическая блокировка закладки, если требуется.
    ?>
    <script language="JavaScript">
        function CheckSchedule() {
            let checked = $('#SCHEDULE_USE').prop('checked');
            $('[id^="SCHEDULE_"]:not(#SCHEDULE_USE)').prop('disabled', !checked);
        }

        $(function () {
            $('#SCHEDULE_USE').change(function () {
                CheckSchedule();
            });
            CheckSchedule();
        });
    </script>

    <?
    // информационная подсказка
    echo BeginNote(); ?>
    <span class="required">*</span><? echo GetMessage("REQUIRED_FIELDS") ?>
    <? echo EndNote(); ?>

    <?
    $oAsset = \Bitrix\Main\Page\Asset::getInstance();
    $oAsset->addJs($modulePath . "/js/script.js");
    ?>
    <? if ($_REQUEST['ID']): ?>
        <script>
            let profileId = <?=$_REQUEST['ID']?>;
            let obWaitWindow = null;
            let nodeContentDetail = document.querySelector('form .adm-detail-content-wrap');
            <?if($str_SETTINGS['STATUS'] == 'RUN'):?>
            $(function () {
                tabControl.SelectTab('edit1');
            });
            <?endif;?>
            BX.addCustomEvent("OnCatalogSyncRefresh", function (data) {
                if (data.hasOwnProperty('import')) {
                    if (data.import.hasOwnProperty(profileId)) {
                        let item = data.import[profileId];
                        let updateKeys = [
                            "status",
                            "last_exec",
                            "result_elements",
                            "result_sections",
                            'errors_msg',
                            'elements_add',
                            'elements_dea',
                            'elements_del',
                            'elements_err',
                            'elements_non',
                            'elements_upd',
                        ];
                        for (let i in updateKeys) {
                            let key = updateKeys[i];
                            let prop = BX('prof_' + profileId + '_' + key);
                            if (!!prop && key.toUpperCase() in item.SETTINGS) {
                                let value = item.SETTINGS[key.toUpperCase()];
                                prop.innerHTML = value;
                            }
                        }
                        if (item.SETTINGS.STATUS_CODE == 'RUN') {
                            if (obWaitWindow === null) {
                                obWaitWindow = BX.showWait(nodeContentDetail);
                            }
                        } else {
                            if (obWaitWindow !== null) {
                                BX.closeWait(document.body, obWaitWindow);
                                obWaitWindow = null;
                            }
                        }
                    }
                }
            });
        </script>
    <? endif; ?>
    <?
    // завершение страницы
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
    ?>
