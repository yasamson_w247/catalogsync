<?php

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\EventManager;

if (!CModule::IncludeModule("iblock"))
    return false;

Loc::loadLanguageFile(__FILE__);

global $USER;

$catalogSyncRights = $APPLICATION->GetGroupRight("catalog.sync");
if ($catalogSyncRights != "D") {
    $eventManager = EventManager::getInstance();
    $eventManager->addEventHandler('main', 'OnBuildGlobalMenu', 'OnBuildGlobalMenu_AddCatalogSyncMenu', false, 10000);

    function OnBuildGlobalMenu_AddCatalogSyncMenu(&$arGlobalMenu, &$arModuleMenu)
    {
        foreach ($arModuleMenu as &$arItem) {
            if ($arItem['items_id'] == "menu_iblock" &&
                $arItem['module_id'] == 'iblock') {
                $arItem['items'][] = array(
                    "parent_menu" => $arItem['items_id'],
                    "section" => "iblock",
                    "sort" => 300,
                    "text" => GetMessage("CATALOG_SYNC_MENU_EXPORT_IMPORT_V2"),
                    "title" => GetMessage("CATALOG_SYNC_MENU_EXPORT_IMPORT_V2_TITLE"),
                    "icon" => "workflow_menu_icon",
                    "page_icon" => "workflow_menu_icon",
                    "items_id" => "catalog_sync_export_import",
                    "module_id" => "catalog.sync",
                    'items' => [
                        [
                            "text" => GetMessage("CATALOG_SYNC_MENU_EXPORT_V2"),
                            "title" => GetMessage("CATALOG_SYNC_MENU_EXPORT_V2_TITLE"),
                            "module_id" => "catalog.sync",
                            "url" => "catalog_sync_export_list.php?lang=" . LANGUAGE_ID,
                            "more_url" => array(
                                "catalog_sync_export_list.php",
                                "catalog_sync_export_edit.php",
                            ),
                        ],
                        [
                            "text" => GetMessage("CATALOG_SYNC_MENU_IMPORT_V2"),
                            "title" => GetMessage("CATALOG_SYNC_MENU_IMPORT_V2_TITLE"),
                            "module_id" => "catalog.sync",
                            "url" => "catalog_sync_import_list.php?lang=" . LANGUAGE_ID,
                            "more_url" => array(
                                "catalog_sync_import_list.php",
                                "catalog_sync_import_edit.php",
                            ),
                        ],
                    ],
                );
            }
        }
        unset($arItem);
    }

}
?>