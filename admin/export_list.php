<?

use \Bitrix\Main\Loader,
    \Bitrix\Main\Localization\Loc,
    Catalog\Sync\Data\Export\ProfileTable as ExportProfileTable;

// подключим все необходимые файлы:
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php"); // первый общий пролог

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/subscribe/include.php"); // инициализация модуля
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/subscribe/prolog.php"); // пролог модуля

// подключим языковой файл
IncludeModuleLangFile(__FILE__);

Loader::includeModule('catalog.sync');

CJSCore::Init(['jquery']);

// получим права доступа текущего пользователя на модуль
$POST_RIGHT = $APPLICATION->GetGroupRight("catalog.sync");
// если нет прав - отправим к форме авторизации с сообщением об ошибке
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$sTableID = "tbl_catalog_sync_profile"; // ID таблицы
$oSort = new CAdminSorting($sTableID, "ID", "desc"); // объект сортировки
$lAdmin = new CAdminList($sTableID, $oSort); // основной объект списка

// ******************************************************************** //
//                           ФИЛЬТР                                     //
// ******************************************************************** //

// *********************** CheckFilter ******************************** //
// проверку значений фильтра для удобства вынесем в отдельную функцию
function CheckFilter()
{
    global $FilterArr, $lAdmin;
    foreach ($FilterArr as $f) global $$f;

    // В данном случае проверять нечего.
    // В общем случае нужно проверять значения переменных $find_имя
    // и в случае возниконовения ошибки передавать ее обработчику
    // посредством $lAdmin->AddFilterError('текст_ошибки').

    return count($lAdmin->arFilterErrors) == 0; // если ошибки есть, вернем false;
}

// *********************** /CheckFilter ******************************* //

// опишем элементы фильтра
$FilterArr = Array(
    "find",
    "find_type",
    "find_id",
    "find_name",
);

// инициализируем фильтр
$lAdmin->InitFilter($FilterArr);

// если все значения фильтра корректны, обработаем его
if (CheckFilter()) {
    // создадим массив фильтрации для выборки CRubric::GetList() на основе значений фильтра
    $arFilter = [];
    if ($find_id) {
        $arFilter['ID'] = $find_id;
    }
    if ($find_active) {
        $arFilter['ACTIVE'] = intval($find_active == 'Y');
    }
    if ($find_name) {
        $arFilter['NAME'] = $find_name;
    }
}


// ******************************************************************** //
//                ОБРАБОТКА ДЕЙСТВИЙ НАД ЭЛЕМЕНТАМИ СПИСКА              //
// ******************************************************************** //

// сохранение отредактированных элементов
if ($lAdmin->EditAction() && $POST_RIGHT == "W") {
    // пройдем по списку переданных элементов
    foreach ($FIELDS as $ID => $arFields) {
        if (!$lAdmin->IsUpdated($ID))
            continue;

        // сохраним изменения каждого элемента
        $DB->StartTransaction();
        $ID = IntVal($ID);
        if (($rsData = ExportProfileTable::getByPrimary($ID)) && ($arData = $rsData->Fetch())) {
            foreach ($arFields as $key => $value)
                $arData[$key] = $value;
            if (!ExportProfileTable::update($ID, $arData)) {
                $lAdmin->AddGroupError(GetMessage("rub_save_error") . " " . $cData->LAST_ERROR, $ID);
                $DB->Rollback();
            }
        } else {
            $lAdmin->AddGroupError(GetMessage("rub_save_error") . " " . GetMessage("rub_no_rubric"), $ID);
            $DB->Rollback();
        }
        $DB->Commit();
    }
}

// обработка одиночных и групповых действий
if (($arID = $lAdmin->GroupAction()) && $POST_RIGHT == "W") {
    // если выбрано "Для всех элементов"
    if ($_REQUEST['action_target'] == 'selected') {
        $cData = new ExportProfileTable();
        $rsData = $cData->getList([
            'filter' => $arFilter,
        ]);
        while ($arRes = $rsData->Fetch())
            $arID[] = $arRes['ID'];
    }

    // пройдем по списку элементов
    foreach ($arID as $ID) {
        if (strlen($ID) <= 0)
            continue;
        $ID = IntVal($ID);

        // для каждого элемента совершим требуемое действие
        switch ($_REQUEST['action']) {
            // удаление
            case "delete":
                @set_time_limit(0);
                $DB->StartTransaction();
                if (!ExportProfileTable::delete($ID)) {
                    $DB->Rollback();
                    $lAdmin->AddGroupError(GetMessage("rub_del_err"), $ID);
                }
                $DB->Commit();
                break;

            // активация/деактивация
            case "activate":
            case "deactivate":
                if (($rsData = ExportProfileTable::getByPrimary($ID)) && ($arFields = $rsData->Fetch())) {
                    $arFields["ACTIVE"] = ($_REQUEST['action'] == "activate" ? "Y" : "N");
                    if (!ExportProfileTable::update($ID, $arFields))
                        $lAdmin->AddGroupError(GetMessage("rub_save_error") . $cData->LAST_ERROR, $ID);
                } else
                    $lAdmin->AddGroupError(GetMessage("rub_save_error") . " " . GetMessage("rub_no_rubric"), $ID);
                break;
        }
    }
}

// ******************************************************************** //
//                ВЫБОРКА ЭЛЕМЕНТОВ СПИСКА                              //
// ******************************************************************** //

// выберем список рассылок
$rsData = ExportProfileTable::getList(array(
    'order' => array($by => $order),
    'filter' => $arFilter
));

// преобразуем список в экземпляр класса CAdminResult
$rsData = new CAdminResult($rsData, $sTableID);

// аналогично CDBResult инициализируем постраничную навигацию.
$rsData->NavStart();

// отправим вывод переключателя страниц в основной объект $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("profile_nav")));

// ******************************************************************** //
//                ПОДГОТОВКА СПИСКА К ВЫВОДУ                            //
// ******************************************************************** //

$lAdmin->AddHeaders(array(
    array("id" => "ID",
        "content" => "ID",
        "sort" => "id",
        "align" => "right",
        "default" => true,
    ),
    array("id" => "NAME",
        "content" => GetMessage("rub_name"),
        "sort" => "name",
        "default" => true,
    ),
    /*array("id" => "LID",
        "content" => GetMessage("rub_site"),
        "sort" => "lid",
        "default" => true,
    ),*/
    array("id" => "ACTIVE",
        "content" => GetMessage("rub_act"),
        "sort" => "act",
        "default" => true,
    ),
    array("id" => "STATUS",
        "content" => GetMessage("rub_state"),
        "default" => true,
    ),
    array("id" => "LAST_EXEC",
        "content" => GetMessage("rub_last_exec"),
        "default" => true,
    ),
));

while ($arRes = $rsData->NavNext(true, "f_")):

    // создаем строку. результат - экземпляр класса CAdminListRow
    $row =& $lAdmin->AddRow($f_ID, $arRes);

    // далее настроим отображение значений при просмотре и редаткировании списка

    // параметр NAME будет редактироваться как текст, а отображаться ссылкой
    $row->AddInputField("NAME", array("size" => 20));
    $row->AddViewField("NAME", '<a href="catalog_sync_export_edit.php?ID=' . $f_ID . '&lang=' . LANG . '">' . $f_NAME . '</a>');

    // флаги ACTIVE и VISIBLE будут редактироваться чекбоксами
    $row->AddCheckField("ACTIVE");

    $row->AddViewField("STATUS", "<span id='prof_{$f_ID}_status'>" . GetMessage("PROFILE_STATUS_" . $f_SETTINGS['STATUS']) . '</span>');
    $row->AddViewField("LAST_EXEC", "<span id='prof_{$f_ID}_last_exec'>" . ($f_SETTINGS['LAST_EXEC'] ? date("d.m.Y H:i:s", $f_SETTINGS['LAST_EXEC']) : "") . '</span>');

    // сформируем контекстное меню
    $arActions = Array();

    // редактирование элемента
    $arActions[] = array(
        "ICON" => "edit",
        "DEFAULT" => true,
        "TEXT" => GetMessage("rub_edit"),
        "ACTION" => $lAdmin->ActionRedirect("catalog_sync_export_edit.php?ID=" . $f_ID)
    );
    // редактирование элемента
    $arActions[] = array(
        "ICON" => "edit",
        "DEFAULT" => true,
        "TEXT" => GetMessage("rub_run"),
        "ACTION" => $lAdmin->ActionRedirect("catalog_sync_export_edit.php?action=run&ID=" . $f_ID)
    );

    // удаление элемента
    if ($POST_RIGHT >= "W")
        $arActions[] = array(
            "ICON" => "delete",
            "TEXT" => GetMessage("rub_del"),
            "ACTION" => "if(confirm('" . GetMessage('rub_del_conf') . "')) " . $lAdmin->ActionDoGroup($f_ID, "delete")
        );

    // вставим разделитель
    $arActions[] = array("SEPARATOR" => true);

    // если последний элемент - разделитель, почистим мусор.
    if (is_set($arActions[count($arActions) - 1], "SEPARATOR"))
        unset($arActions[count($arActions) - 1]);

    // применим контекстное меню к строке
    $row->AddActions($arActions);

endwhile;

// резюме таблицы
$lAdmin->AddFooter(
    array(
        array("title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value" => $rsData->SelectedRowsCount()), // кол-во элементов
        array("counter" => true, "title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value" => "0"), // счетчик выбранных элементов
    )
);

// групповые действия
$lAdmin->AddGroupActionTable(Array(
    "delete" => GetMessage("MAIN_ADMIN_LIST_DELETE"), // удалить выбранные элементы
    "activate" => GetMessage("MAIN_ADMIN_LIST_ACTIVATE"), // активировать выбранные элементы
    "deactivate" => GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"), // деактивировать выбранные элементы
));

// ******************************************************************** //
//                АДМИНИСТРАТИВНОЕ МЕНЮ                                 //
// ******************************************************************** //

// сформируем меню из одного пункта - добавление рассылки
$aContext = array(
    array(
        "TEXT" => GetMessage("POST_ADD"),
        "LINK" => "catalog_sync_export_edit.php?lang=" . LANG,
        "TITLE" => GetMessage("POST_ADD_TITLE"),
        "ICON" => "btn_new",
    ),
);

// и прикрепим его к списку
$lAdmin->AddAdminContextMenu($aContext);

// ******************************************************************** //
//                ВЫВОД                                                 //
// ******************************************************************** //

// альтернативный вывод
$lAdmin->CheckListMode();

// установим заголовок страницы
$APPLICATION->SetTitle(GetMessage("rub_title"));

// не забудем разделить подготовку данных и вывод
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

// ******************************************************************** //
//                ВЫВОД ФИЛЬТРА                                         //
// ******************************************************************** //

// создадим объект фильтра
$oFilter = new CAdminFilter(
    $sTableID . "_filter",
    array(
        "ID",
        GetMessage("rub_f_active"),
    )
);
?>
    <form name="find_form" method="get" action="<? echo $APPLICATION->GetCurPage(); ?>">
        <? $oFilter->Begin(); ?>
        <tr>
            <td><?= "ID" ?>:</td>
            <td>
                <input type="text" name="find_id" size="47" value="<? echo htmlspecialchars($find_id) ?>">
            </td>
        </tr>
        <tr>
            <td><?= GetMessage("rub_f_active") ?>:</td>
            <td>
                <?
                $arr = array(
                    "reference" => array(
                        GetMessage("POST_YES"),
                        GetMessage("POST_NO"),
                    ),
                    "reference_id" => array(
                        "Y",
                        "N",
                    )
                );
                echo SelectBoxFromArray("find_active", $arr, $find_active, GetMessage("POST_ALL"), "");
                ?>
            </td>
        </tr>
        <?
        $oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage(), "form" => "find_form"));
        $oFilter->End();
        ?>
    </form>

<?
// выведем таблицу списка элементов
$lAdmin->DisplayList();
?>

<?
// завершение страницы
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>