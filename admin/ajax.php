<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Catalog\Sync\Data\Import\ProfileTable as ImportProfileTable;
use Catalog\Sync\Data\Export\ProfileTable as ExportProfileTable;
use \Bitrix\Main\Localization\Loc;

\Bitrix\Main\Loader::includeModule('catalog.sync');

Loc::loadLanguageFile(__FILE__);
Loc::loadLanguageFile(__DIR__ . '/export_edit.php');
Loc::loadLanguageFile(__DIR__ . '/import_edit.php');

$app = \Bitrix\Main\Application::getInstance();
$documentRoot = \Bitrix\Main\Application::getDocumentRoot();
$context = $app->getContext();
$request = $context->getRequest();

$modulePath = getLocalPath("modules/catalog.sync");

$result = [];

$psList = \Catalog\Sync\System::ps();
$result['ps'] = array_filter($psList, function ($v, $k) use ($documentRoot, $modulePath) {
    if (strpos($v['cmd'], "php") !== false && (
            strpos($v['cmd'], "-f {$documentRoot}{$modulePath}/lib/cli/import.php") !== false ||
            strpos($v['cmd'], "-f {$documentRoot}{$modulePath}/lib/cli/export.php") !== false
        )) {
        return true;
    }
    return false;
}, ARRAY_FILTER_USE_BOTH);

$dbRes = ImportProfileTable::getList([]);
while ($arRes = $dbRes->fetch()) {
    foreach ($result['ps'] as $arPs) {
        if (strpos($arPs['cmd'], "-f {$documentRoot}{$modulePath}/lib/cli/import.php") !== false) {
            if (strpos($arPs['cmd'], "--id={$arRes['ID']}" !== false)) {
                $arRes['PROCESS'] = $arPs;
                break;
            }
        }
    }

    if (!$arRes['PROCESS'] &&
        $arRes['SETTINGS']['STATUS'] &&
        !($arRes['SETTINGS']['STATUS'] == 'FINISH' || $arRes['SETTINGS']['STATUS'] == 'ERROR')) {
        $arRes['SETTINGS']['STATUS'] = 'ERROR';
        $arRes['SETTINGS']['ERRORS'][] = Loc::getMessage('SYSTEM_ERROR');
    }
    $arRes['SETTINGS']['ERRORS_MSG'] = implode('<br>', $arRes['SETTINGS']['ERRORS']);
    $arRes['SETTINGS']['LAST_EXEC_TIMESTAMP'] = $arRes['SETTINGS']['LAST_EXEC'];
    $arRes['SETTINGS']['STATUS_CODE'] = $arRes['SETTINGS']['STATUS'];
    $arRes['SETTINGS']['STATUS'] = Loc::getMessage("PROFILE_STATUS_" . $arRes['SETTINGS']['STATUS']);
    $arRes['SETTINGS']['LAST_EXEC'] = $arRes['SETTINGS']['LAST_EXEC'] ? date('d.m.Y, H:i:s', $arRes['SETTINGS']['LAST_EXEC']) : "";
    $result['import'][$arRes['ID']] = $arRes;
}

$dbRes = ExportProfileTable::getList([]);
while ($arRes = $dbRes->fetch()) {
    foreach ($result['ps'] as $arPs) {
        if (strpos($arPs['cmd'], "-f {$documentRoot}{$modulePath}/lib/cli/export.php") !== false) {
            if (strpos($arPs['cmd'], "--id={$arRes['ID']}" !== false)) {
                $arRes['PROCESS'] = $arPs;
                break;
            }
        }
    }

    if (!$arRes['PROCESS'] &&
        $arRes['SETTINGS']['STATUS'] &&
        !($arRes['SETTINGS']['STATUS'] == 'FINISH' || $arRes['SETTINGS']['STATUS'] == 'ERROR')) {
        $arRes['SETTINGS']['STATUS'] = 'ERROR';
        $arRes['SETTINGS']['ERRORS'][] = Loc::getMessage('SYSTEM_ERROR');
    }

    $arRes['SETTINGS']['ERRORS_MSG'] = implode('<br>', $arRes['SETTINGS']['ERRORS']);
    $arRes['SETTINGS']['LAST_EXEC_TIMESTAMP'] = $arRes['SETTINGS']['LAST_EXEC'];
    $arRes['SETTINGS']['STATUS_CODE'] = $arRes['SETTINGS']['STATUS'];
    $arRes['SETTINGS']['STATUS'] = Loc::getMessage("PROFILE_STATUS_" . $arRes['SETTINGS']['STATUS']);
    $arRes['SETTINGS']['LAST_EXEC'] = $arRes['SETTINGS']['LAST_EXEC'] ? date('d.m.Y, H:i:s', $arRes['SETTINGS']['LAST_EXEC']) : "";
    $arRes['SETTINGS']['RESULT_SECTIONS'] = $arRes['SETTINGS']['RESULT']['SECTIONS'];
    $arRes['SETTINGS']['RESULT_ELEMENTS'] = $arRes['SETTINGS']['RESULT']['ELEMENTS'];
    $result['export'][$arRes['ID']] = $arRes;
}

echo \Bitrix\Main\Web\Json::encode($result);

die();
?>
