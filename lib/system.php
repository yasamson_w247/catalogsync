<?php

namespace Catalog\Sync;

class System
{
    public static function ps()
    {
        $psList = [];
        $pMask = [
            'pid' => 20,
            'user' => 20,
            'stat' => 5,
            'time' => 20,
            'cmd' => 0,
        ];
        $pMaskStr = [];
        array_walk($pMask, function ($value, $key) use (&$pMaskStr) {
            if (intval($value) > 0) {
                $pMaskStr[] = "{$key}:{$value}";
            } else {
                $pMaskStr[] = "{$key}";
            }
        });
        $pMaskStr = implode(',', $pMaskStr);
        $cmd = "ps --no-header --cols 1000 -eo {$pMaskStr}";
        exec($cmd, $output);
        foreach ($output as $row) {
            $offset = 0;
            $arPs = [];
            foreach ($pMask as $name => $length) {
                $length = intval($length);
                if ($length > 0) {
                    $arPs[$name] = mb_substr($row, $offset, $length);
                } else {
                    $arPs[$name] = mb_substr($row, $offset);
                }
                $offset += $length + 1;
            }
            if ($arPs) {
                array_walk($arPs, function (&$item) {
                    $item = trim($item);
                });
                $psList[] = $arPs;
            }
        }

        return $psList;
    }
}