<?php

namespace Catalog\Sync\Agent;

use Bitrix\Main\Type\DateTime;
use Catalog\Sync\Cron\CronExpression;
use Catalog\Sync\Data\Export\ProfileTable as ExportProfileTable;
use Catalog\Sync\Data\Import\ProfileTable as ImportProfileTable;
use Catalog\Sync\Export;
use Catalog\Sync\Import;

class Runner
{
    public static function checkProfiles()
    {
        $exportList = ExportProfileTable::getList([
            'filter' => ['ACTIVE' => 1],
        ])->fetchAll();
        foreach ($exportList as $item) {
            if (self::isDue($item)) {
                Export::run($item['ID']);
            }
        }

        $importList = ImportProfileTable::getList([
            'filter' => ['ACTIVE' => 1],
        ])->fetchAll();
        foreach ($importList as $item) {
            if (self::isDue($item)) {
                Import::run($item['ID']);
            }
        }

        return "\Catalog\Sync\Agent\Runner::checkProfiles();";
    }

    public static function isScheduleUse(array $arProfile): bool
    {
        return (bool)$arProfile['SETTINGS']['SCHEDULE']['USE'] == 'Y';
    }

    public static function getSchedule(array $arProfile): string
    {
        $cronStr = implode(" ", [
            $arProfile['SETTINGS']['SCHEDULE']['MINUTES'] ?: "*",
            $arProfile['SETTINGS']['SCHEDULE']['HOURS'] ?: "*",
            $arProfile['SETTINGS']['SCHEDULE']['DAYS'] ?: "*",
            $arProfile['SETTINGS']['SCHEDULE']['MONTHS'] ?: "*",
            $arProfile['SETTINGS']['SCHEDULE']['DAYS_OF_WEEK'] ?: "*",
        ]);
        return $cronStr;
    }

    public static function getLastExec(array $arProfile): string
    {
        return (string)date("d.m.Y H:i:s", $arProfile['SETTINGS']['LAST_EXEC']);
    }

    public static function isRun(array $arProfile): string
    {
        return (string)$arProfile['SETTINGS']['RUN'] == 'Y';
    }

    public static function isDue(array $arProfile, int $checkStartDateTime = null): bool
    {
        static $INTERVAL = null;
        if($INTERVAL === null) {
            $agentInterval = \CAgent::GetList(["ID" => "DESC"], ["NAME" => "\Catalog\Sync\Agent\Runner::checkProfiles();", 'MODULE_ID' => 'catalog.sync'])->Fetch();
            $INTERVAL = intval($agentInterval['AGENT_INTERVAL']) ?: 60;
        }

        if ($checkStartDateTime === null) {
            $checkStartDateTime = new DateTime();
        }
        $checkStartTimestamp = $checkStartDateTime->getTimestamp();

        if (!self::isRun($arProfile)) {
            if (self::isScheduleUse($arProfile)) {
                $lastExec = self::getLastExec($arProfile);
                $cronStr = self::getSchedule($arProfile);
                $obCron = CronExpression::factory($cronStr);
                $nextExecDateTime = $obCron->getNextRunDate($lastExec ?: ConvertTimeStamp(false, 'FULL'));
                if($nextExecDateTime->getTimestamp() < time()) {
                    $nextExecDateTime = new \DateTime();
                }
                $nextExecTimestamp = $nextExecDateTime->getTimestamp();
                if (($nextExecTimestamp < $checkStartTimestamp && $nextExecTimestamp + $INTERVAL >= $checkStartTimestamp) ||
                    ($nextExecTimestamp > $checkStartTimestamp && $nextExecTimestamp - $INTERVAL <= $checkStartTimestamp) ||
                    $nextExecTimestamp == $checkStartTimestamp) {

                    return true;
                }
            }
        }

        return false;
    }
}