<?
namespace Catalog\Sync;

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\SystemException;
use Catalog\Sync\Data\Import\ProfileTable as ImportProfileTable;

class Import {
    public static function run($profileId) {
        $obThred = new Threads();

        $app = \Bitrix\Main\Application::getInstance();
        $context = $app->getContext();
        $request = $context->getRequest();
        $documentRoot = Application::getDocumentRoot();

        $obCatalogSync = \CModule::CreateModuleObject('catalog.sync');

        $arProfile = ImportProfileTable::getList([
            'filter' => ['ID' => $profileId],
        ])->fetch();
        if (!$arProfile) {
            throw new SystemException("Profile with ID {$profileId} not found");
        }

        $filename = $documentRoot . $obCatalogSync->getModuleDir() . '/lib/cli/import.php';
        $arParams = [
            'ID' => $profileId,
            'DOCUMENT_ROOT' => $documentRoot,
            'PROFILE_ID' => $profileId,
            'IBLOCK_TYPE' => $arProfile['SETTINGS']['IBLOCK_TYPE'],
            'IBLOCK_ID' => $arProfile['SETTINGS']['IBLOCK_ID'],
            'URL_DATA_FILE' => $arProfile['SETTINGS']['URL_DATA_FILE'],
            'LID' => $arProfile['SETTINGS']['LID'],
            'USE_CRC' => $arProfile['SETTINGS']['USE_CRC'],
            'outFileAction' => $arProfile['SETTINGS']['outFileAction'],
            'GENERATE_PREVIEW' => $arProfile['SETTINGS']['GENERATE_PREVIEW'],
            'TIMEZONE' => ini_get('date.timezone'),
            'DEBUG' => Option::get("catalog.sync", "DEBUG", "N"),
            'DEBUG_FILENAME' => Option::get("catalog.sync", "DEBUG_FILENAME", ""),
            'XDEBUG' => Option::get("catalog.sync", "XDEBUG", "N"),
            'XDEBUG_REMOTE_PORT' => Option::get("catalog.sync", "XDEBUG_REMOTE_PORT", ""),
        ];

        $obThred->newThread($filename, $arParams);
    }
}
?>
