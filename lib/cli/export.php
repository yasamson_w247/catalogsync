<?php

use \Catalog\Sync\Data\Export\ProfileTable;
use Catalog\Sync\Threads;

function shutdown()
{
    if ($arError = error_get_last()) {
        __d($arError, "----- SHUTDOWN ERRORS -----");
    }
}

register_shutdown_function('shutdown');

require_once dirname(__DIR__) . "/threads.php";

$argList = Threads::getParams();

foreach ($argList as $key => $value) {
    $_REQUEST[$key] = $value;
}

if (isset($argList['TIMEZONE'])) {
    ini_set('date.timezone', $argList['TIMEZONE']);
}

$_SERVER['DOCUMENT_ROOT'] = $DOCUMENT_ROOT = $argList['DOCUMENT_ROOT'];
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\Loader::includeModule("iblock");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iblock/prolog.php");

// ----- DEBUG ------
global $debug, $debugFilename;
$debug = $argList['DEBUG'] == 'Y';
$debugFilename = (string)$argList['DEBUG_FILENAME'];

function __d($var, $varName)
{
    global $debug, $debugFilename;
    if($debug) {
        \Bitrix\Main\Diag\Debug::dumpToFile($var, $varName, $debugFilename);
    }
}

// ----- DEBUG ------

__d(date("d.m.Y H:i:s"), "----- START EXPORT -----");
__d($argList, "----- CLI ARGS -----");

ini_set('memory_limit', '-1');
set_time_limit(0);

$arErrors = array();
$arMessages = array();

if (array_key_exists("NS", $_REQUEST) && is_array($_REQUEST["NS"]))
    $NS = $_REQUEST["NS"];
else
    $NS = array(
        "IBLOCK_ID" => $_REQUEST["IBLOCK_ID"],
        "URL_DATA_FILE" => $_REQUEST["URL_DATA_FILE"],
        "SECTIONS_FILTER" => $_REQUEST["SECTIONS_FILTER"],
        "ELEMENTS_FILTER" => $_REQUEST["ELEMENTS_FILTER"],
        "DOWNLOAD_CLOUD_FILES" => $_REQUEST["DOWNLOAD_CLOUD_FILES"] === "N" ? "N" : "Y",
    );

$NS["INTERVAL"] = $_REQUEST["INTERVAL"] ?: 5;
$NS["catalog"] = CModule::IncludeModule('catalog');
$NS["catalog.sync"] = CModule::IncludeModule('catalog.sync');

$arProfile = ProfileTable::getByPrimary($_REQUEST['PROFILE_ID'])->fetch();
$arProfile['SETTINGS']['STATUS'] = 'RUN';
$arProfile['SETTINGS']['LAST_EXEC'] = time();
$arProfile['SETTINGS']['ERRORS'] = [];
$arProfile['SETTINGS']['RESULT']['ELEMENTS'] = 0;
$arProfile['SETTINGS']['RESULT']['SECTIONS'] = 0;
ProfileTable::update(
    $_REQUEST['PROFILE_ID'],
    [
        'SETTINGS' => $arProfile['SETTINGS'],
    ]
);


//We have to strongly check all about file names at server side
$ABS_FILE_NAME = false;
$WORK_DIR_NAME = false;
$FILE_DIR_NAME = false;
$NEXT_STEP = [];

if (isset($NS["URL_DATA_FILE"]) && (strlen($NS["URL_DATA_FILE"]) > 0)) {
    $filename = trim(str_replace("\\", "/", trim($NS["URL_DATA_FILE"])), "/");
    if (
        preg_match('/[^a-zA-Z0-9\s!#\$%&\(\)\[\]\{\}+\.;=@\^_\~\/\\\\\-]/i', $filename)
        || HasScriptExtension($filename)
    ) {
        $arErrors[] = GetMessage("IBLOCK_CML2_FILE_NAME_ERROR");
    } else {
        $FILE_NAME = rel2abs($_SERVER["DOCUMENT_ROOT"], "/" . $filename);
        if ((strlen($FILE_NAME) > 1) && ($FILE_NAME === "/" . $filename)) {
            $ABS_FILE_NAME = $_SERVER["DOCUMENT_ROOT"] . $FILE_NAME;
            if (strtolower(substr($ABS_FILE_NAME, -4)) != ".xml")
                $ABS_FILE_NAME .= ".xml";
            $WORK_DIR_NAME = substr($ABS_FILE_NAME, 0, strrpos($ABS_FILE_NAME, "/") + 1);
        }
    }
}

$fp = false;
if ($ABS_FILE_NAME && (count($arErrors) == 0)) {

    $PROPERTY_MAP = false;
    $SECTION_MAP = false;
    $PRICES_MAP = false;

    if ($fp = fopen($ABS_FILE_NAME, "wb")) {
        if (strtolower(substr($ABS_FILE_NAME, -4)) == ".xml") {
            $DIR_NAME = substr($ABS_FILE_NAME, 0, -4) . "_files";
            if (
                is_dir($DIR_NAME)
                || @mkdir($DIR_NAME, BX_DIR_PERMISSIONS)
            ) {
                $FILE_DIR_NAME = substr($DIR_NAME . "/", strlen($WORK_DIR_NAME));
            }
        }
    } else {
        $arErrors[] = GetMessage("IBLOCK_CML2_FILE_ERROR");
    }

    if ($fp = fopen($ABS_FILE_NAME, "ab")) {
        $obExport = new CIBlockCMLExport;

        if ($obExport->Init($fp, $NS["IBLOCK_ID"], $NEXT_STEP, true, $WORK_DIR_NAME, $FILE_DIR_NAME, false)) {
            if ($NS["DOWNLOAD_CLOUD_FILES"] === "N")
                $obExport->DoNotDownloadCloudFiles();

            $obExport->StartExport();
            $obExport->StartExportMetadata();
            $obExport->ExportProperties($PROPERTY_MAP);

            do {
                $result = $obExport->ExportSections(
                    $SECTION_MAP,
                    $start_time,
                    $NS['INTERVAL'],
                    $NS["SECTIONS_FILTER"],
                    $PROPERTY_MAP
                );
                if ($result) {
                    $NS["SECTIONS"] += $result;
                } else {
                    $obExport->EndExportMetadata();
                    $obExport->StartExportCatalog();
                }
                $arProfile['SETTINGS']['RESULT']['SECTIONS'] = $NS["SECTIONS"];
                ProfileTable::update(
                    $_REQUEST['PROFILE_ID'],
                    [
                        'SETTINGS' => $arProfile['SETTINGS'],
                    ]
                );
            } while ($result);

            do {
                $result = $obExport->ExportElements(
                    $PROPERTY_MAP,
                    $SECTION_MAP,
                    $start_time,
                    $NS['INTERVAL'],
                    0,
                    $NS["ELEMENTS_FILTER"]
                );

                if ($result) {
                    $NS["ELEMENTS"] += $result;
                } else {
                    $obExport->EndExportCatalog();
                    $obExport->ExportProductSets();
                    $obExport->EndExport();
                }
                $arProfile['SETTINGS']['RESULT']['ELEMENTS'] = $NS["ELEMENTS"];
                ProfileTable::update(
                    $_REQUEST['PROFILE_ID'],
                    [
                        'SETTINGS' => $arProfile['SETTINGS'],
                    ]
                );
            } while ($result);


        } else {
            $arErrors[] = GetMessage("IBLOCK_CML2_IBLOCK_ERROR");
        }
    } else {
        $arErrors[] = GetMessage("IBLOCK_CML2_FILE_ERROR") . "(1)";
    }
} else {
    $arErrors[] = GetMessage("IBLOCK_CML2_FILE_ERROR") . "(2)";
}

if ($fp)
    fclose($fp);

if ($arErrors) {
    $arProfile['SETTINGS']['ERRORS'] = $arErrors;

}
$arProfile['SETTINGS']['STATUS'] = 'FINISH';

ProfileTable::update(
    $_REQUEST['PROFILE_ID'],
    [
        'SETTINGS' => $arProfile['SETTINGS'],
    ]
);

__d($arProfile, "----- PROFILE DATA -----");
__d($arErrors, "----- ERRORS -----");
__d(date("d.m.Y H:i:s"), "----- END EXPORT -----");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>
