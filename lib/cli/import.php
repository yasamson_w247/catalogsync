<?

use \Catalog\Sync\Data\Import\ProfileTable,
    \Bitrix\Iblock\IblockTable,
    \Bitrix\Main\Localization\Loc,
    \Catalog\Sync\Threads;

function shutdown()
{
    if ($arError = error_get_last()) {
        __d($arError, "----- SHUTDOWN ERRORS -----");
    }
}

register_shutdown_function('shutdown');

require_once dirname(__DIR__) . "/threads.php";

$argList = Threads::getParams();

foreach ($argList as $key => $value) {
    $_REQUEST[$key] = $value;
}

if (isset($argList['TIMEZONE'])) {
    ini_set('date.timezone', $argList['TIMEZONE']);
}
ini_set('mbstring.func_overload', 0);
$_SERVER['DOCUMENT_ROOT'] = $DOCUMENT_ROOT = $argList['DOCUMENT_ROOT'];
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\Loader::includeModule("iblock");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/iblock/prolog.php");

// ----- DEBUG ------
global $debug, $debugFilename;
$debug = $argList['DEBUG'] == 'Y';
$debugFilename = (string)$argList['DEBUG_FILENAME'];

function __d($var, $varName)
{
    global $debug, $debugFilename;
    if ($debug) {
        \Bitrix\Main\Diag\Debug::dumpToFile($var, $varName, $debugFilename);
    }
}

// ----- DEBUG ------

__d(date("d.m.Y H:i:s"), "----- START EXPORT -----");
__d($argList, "----- CLI ARGS -----");

Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/iblock/admin/iblock_xml_import.php');
Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/iblock/classes/general/cml2.php');

@ini_set('memory_limit', '-1');
@set_time_limit(0);

$start_time = time();

$arErrors = array();
$arMessages = array();

//Initialize NS variable which will save step data
if (array_key_exists("NS", $_REQUEST) && is_array($_REQUEST["NS"])) {
    $NS = $_REQUEST["NS"];
    if (array_key_exists("charset", $NS) && $NS["charset"] === "false") $NS["charset"] = false;
    if (array_key_exists("PREVIEW", $NS) && $NS["PREVIEW"] === "false") $NS["PREVIEW"] = false;
    if (array_key_exists("bOffer", $NS) && $NS["bOffer"] === "false") $NS["bOffer"] = false;
} else {
    $NS = array(
        "STEP" => 0,
        "IBLOCK_TYPE" => $_REQUEST["IBLOCK_TYPE"],
        "LID" => is_array($_REQUEST["LID"]) ? $_REQUEST["LID"] : array(),
        "URL_DATA_FILE" => $_REQUEST["URL_DATA_FILE"],
        "ACTION" => $_REQUEST["outFileAction"],
        "PREVIEW" => $_REQUEST["GENERATE_PREVIEW"] === "Y",
        "USE_CRC" => $_REQUEST["USE_CRC"] === "Y",
    );
}

$INTERVAL = $_REQUEST['INTERVAL'] ?: 30;

$NS["INTERVAL"] = $_REQUEST["INTERVAL"] ?: 5;
$NS["catalog"] = CModule::IncludeModule('catalog');
$NS["catalog.sync"] = CModule::IncludeModule('catalog.sync');

function SetProfileData($profileId, $arData = array())
{
    static $arProfile = null;
    if ($arProfile === null) {
        $arProfile = ProfileTable::getByPrimary($profileId)->fetch();
    }
    if ($arData && $arProfile) {
        if ($arData['SETTINGS']) {
            $settings = $arData['SETTINGS'] ?: array();
            unset($arData['SETTINGS']);
            $arProfile['SETTINGS'] = array_merge($arProfile['SETTINGS'], $settings);
        }
        $arProfile = array_merge($arProfile, $arData);
        ProfileTable::update($profileId, $arProfile);
    }
}

SetProfileData($_REQUEST['PROFILE_ID'], [
    'SETTINGS' => [
        'ERRORS' => [],
        'STATUS' => 'RUN',
        'LAST_EXEC' => time(),
        'ELEMENTS_UPD' => 0,
        'ELEMENTS_ADD' => 0,
        'ELEMENTS_ERR' => 0,
        'ELEMENTS_DEL' => 0,
        'ELEMENTS_DEA' => 0,
        'ELEMENTS_NON' => 0,
    ],
]);

$dirTmp = $_SERVER['DOCUMENT_ROOT'] . '/upload/tmp/';
CheckDirPath($dirTmp);

//We have to strongly check all about file names at server side
$ABS_FILE_NAME = false;
$WORK_DIR_NAME = false;
$REMOTE_PATH = false;
if (isset($NS["URL_DATA_FILE"]) && (strlen($NS["URL_DATA_FILE"]) > 0)) {

    if (preg_match('/^(http(s)?:\/\/)|\/\//', $NS["URL_DATA_FILE"])) {
        $baseFilename = basename($NS["URL_DATA_FILE"]);
        $REMOTE_PATH = substr($NS['URL_DATA_FILE'], 0, -mb_strlen($baseFilename));

        $ch = curl_init($NS["URL_DATA_FILE"]);
        $fp = fopen($dirTmp . $baseFilename, 'w');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
        $NS["URL_DATA_FILE"] = str_replace($_SERVER['DOCUMENT_ROOT'], '', $dirTmp . $baseFilename);
        $IS_REMOTE_HOST = true;
    }

    $filename = trim(str_replace("\\", "/", trim($NS["URL_DATA_FILE"])), "/");
    $FILE_NAME = rel2abs($_SERVER["DOCUMENT_ROOT"], "/" . $filename);
    if ((strlen($FILE_NAME) > 1) && ($FILE_NAME === "/" . $filename)) {
        $ABS_FILE_NAME = $_SERVER["DOCUMENT_ROOT"] . $FILE_NAME;
        $WORK_DIR_NAME = substr($ABS_FILE_NAME, 0, strrpos($ABS_FILE_NAME, "/") + 1);
    }
}


//This object will load file into database
$obXMLFile = new CIBlockXMLFile;


$IBLOCK_ID = $_REQUEST['IBLOCK_ID'];

$xmlId = null;
if ($IBLOCK_ID) {
    $arIblock = IblockTable::getByPrimary($IBLOCK_ID)->fetch();
    if (!$arIblock['XML_ID']) {
        $xmlId = $arIblock['IBLOCK_TYPE_ID'] . "_" . $IBLOCK_ID;
        IblockTable::update($IBLOCK_ID, ['XML_ID' => $xmlId]);
    } else {
        $xmlId = $arIblock['XML_ID'];
    }
}

if ($ABS_FILE_NAME) {
    // ----------------------------
    //This will save mapping for ID to XML_ID translation
    $SECTION_MAP = null;
    $PRICES_MAP = null;

    CIBlockXMLFile::DropTemporaryTables();
    if (!CIBlockCMLImport::CheckIfFileIsCML($ABS_FILE_NAME))
        $arErrors[] = GetMessage("IBLOCK_CML2_WRONG_FILE_ERROR");
    // ----------------------------
    if (!CIBlockXMLFile::CreateTemporaryTables())
        $arErrors[] = GetMessage("IBLOCK_CML2_TABLE_CREATE_ERROR");
    // ----------------------------
    if (file_exists($ABS_FILE_NAME) && is_file($ABS_FILE_NAME) && ($fp = fopen($ABS_FILE_NAME, "rb"))) {
        SetProfileData($_REQUEST['PROFILE_ID'], [
            'SETTINGS' => [
                'STATUS' => 'READ_XML_TO_DATABASE',
            ],
        ]);
        $obXMLFile->ReadXMLToDatabase($fp, $NS, 0);
        fclose($fp);
    } else {
        $arErrors[] = GetMessage("IBLOCK_CML2_FILE_ERROR");
    }
    // ----------------------------
    if (!CIBlockXMLFile::IndexTemporaryTables())
        $arErrors[] = GetMessage("IBLOCK_CML2_INDEX_ERROR");
    // ----------------------------

    if ($xmlId) {
        $arLevel0 = $DB->Query("SELECT * FROM b_xml_tree WHERE NAME='" . Loc::getMessage('IBLOCK_XML2_COMMERCE_INFO') . "' AND DEPTH_LEVEL = 0")->Fetch();
        if ($arLevel0) {
            $arLevel1 = $DB->Query("SELECT * FROM b_xml_tree WHERE NAME='" . Loc::getMessage('IBLOCK_XML2_METADATA') . "' AND PARENT_ID = {$arLevel0['ID']}")->Fetch();
            $arLevel2 = $DB->Query("SELECT * FROM b_xml_tree WHERE NAME='" . Loc::getMessage('IBLOCK_XML2_CATALOG') . "' AND PARENT_ID = {$arLevel0['ID']}")->Fetch();
        }
        if ($arLevel1) {
            $arLevel5 = $DB->Query("SELECT * FROM b_xml_tree WHERE NAME='" . Loc::getMessage('IBLOCK_XML2_ID') . "' AND PARENT_ID = {$arLevel1['ID']}")->Fetch();
        }
        if ($arLevel2) {
            $arLevel3 = $DB->Query("SELECT * FROM b_xml_tree WHERE NAME='" . Loc::getMessage('IBLOCK_XML2_ID') . "' AND PARENT_ID = {$arLevel2['ID']}")->Fetch();
            $arLevel4 = $DB->Query("SELECT * FROM b_xml_tree WHERE NAME='" . Loc::getMessage('IBLOCK_XML2_METADATA_ID') . "' AND PARENT_ID = {$arLevel2['ID']}")->Fetch();
        }
        $xmlIdList = array_filter([$arLevel5['ID'], $arLevel3['ID'], $arLevel4['ID']]);
        if ($xmlIdList) {
            $xmlIdList = implode(',', $xmlIdList);
            $DB->Query("UPDATE b_xml_tree SET VALUE = '{$xmlId}' WHERE ID IN({$xmlIdList})");
        }
    }

    if ($REMOTE_PATH) {
        $imgDir = $DB->ForSql(substr(basename($ABS_FILE_NAME), 0, -4) . '_files/');
        $imgDBRes = $DB->Query("SELECT * FROM b_xml_tree WHERE VALUE LIKE \"{$imgDir}%\"");
        while ($imgArRes = $imgDBRes->Fetch()) {
            if ($imgArRes['VALUE']) {
                $imgAbsFilename = $dirTmp . $imgArRes['VALUE'];
                if (!file_exists($imgAbsFilename)) {
                    CheckDirPath(dirname($imgAbsFilename) . "/");
                    $ch = curl_init($REMOTE_PATH . $imgArRes['VALUE']);
                    $fp = fopen($imgAbsFilename, 'w');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);
                }
            }
        }
    }

    // ----------------------------
    $obCatalog = new CIBlockCMLImport;
    $obCatalog->Init($NS, $WORK_DIR_NAME, true, $NS["PREVIEW"], false, true);
    $result = $obCatalog->ImportMetaData(array(1, 2), $NS["IBLOCK_TYPE"], $NS["LID"]);
    if ($result === true) {
        $result = $obCatalog->ImportSections();
        if ($result === true)
            $obCatalog->DeactivateSections("A");
    }

    if ($result !== true)
        $arErrors[] = $result;
    // ----------------------------
    do {
        if (($NS["DONE"]["ALL"] <= 0) && $NS["XML_ELEMENTS_PARENT"]) {
            $rs = $DB->Query("select count(*) C from b_xml_tree where PARENT_ID = " . intval($NS["XML_ELEMENTS_PARENT"]));
            $ar = $rs->Fetch();
            $NS["DONE"]["ALL"] = $ar["C"];
        }

        $obCatalog = new CIBlockCMLImport;
        $obCatalog->Init($NS, $WORK_DIR_NAME, $NS['USE_CRC'], $NS["PREVIEW"], false, true);
        $obCatalog->ReadCatalogData($SECTION_MAP, $PRICES_MAP);
        $result = $obCatalog->ImportElements($start_time, $INTERVAL);

        $counter = 0;
        foreach ($result as $key => $value) {
            $NS["DONE"][$key] += $value;
            $counter += $value;
        }
        SetProfileData($_REQUEST['PROFILE_ID'], [
            'SETTINGS' => [
                'STATUS' => 'PROGRESS_IMPORT_ELEMENTS',
                'ELEMENTS_UPD' => $NS["DONE"]['UPD'],
                'ELEMENTS_ADD' => $NS["DONE"]['ADD'],
                'ELEMENTS_ERR' => $NS["DONE"]['ERR'],
            ],
        ]);
    } while ($counter);
    // ----------------------------
    do {
        $obCatalog = new CIBlockCMLImport;
        $obCatalog->Init($NS, $WORK_DIR_NAME, true, $NS["PREVIEW"], false, true);
        $result = $obCatalog->DeactivateElement($NS["ACTION"], $start_time, $INTERVAL);

        $counter = 0;
        foreach ($result as $key => $value) {
            $NS["DONE"][$key] += $value;
            $counter += $value;
        }
        SetProfileData($_REQUEST['PROFILE_ID'], [
            'SETTINGS' => [
                'STATUS' => 'PROGRESS_IMPORT_ELEMENTS_DEACTIVATE',
                'ELEMENTS_DEL' => $NS["DONE"]['DEA'],
                'ELEMENTS_DEA' => $NS["DONE"]['DEA'],
                'ELEMENTS_NON' => $NS["DONE"]['NON'],
            ],
        ]);
    } while ($counter);
    // ----------------------------
    $obCatalog = new CIBlockCMLImport;
    $obCatalog->Init($NS, $WORK_DIR_NAME, true, $NS["PREVIEW"], false, true);
    $obCatalog->ImportProductSets();
} else {
    $arErrors[] = GetMessage("IBLOCK_CML2_FILE_ERROR");
}

if ($arErrors) {
    $arData = [
        'SETTINGS' => [
            'ERRORS' => $arErrors,
        ]
    ];
}
$arData = [
    'SETTINGS' => [
        'STATUS' => 'FINISH',
    ]
];

SetProfileData($_REQUEST['PROFILE_ID'], $arData);

__d($arProfile, "----- PROFILE DATA -----");
__d($arErrors, "----- ERRORS -----");
__d(date("d.m.Y H:i:s"), "----- END EXPORT -----");

die();

//require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>
