<?php

namespace Catalog\Sync;

use Catalog\Sync\ThreadsException;

class Threads
{
    public $phpPath = 'php';

    private $lastId = 0;
    private $descriptorSpec = array(
        0 => array('pipe', 'r'),
        1 => array('pipe', 'w')
    );
    private $handles = array();
    private $streams = array();
    private $results = array();
    private $pipes = array();
    private $timeout = 30;

    public function newThread($filename, $params = array())
    {
        if (!file_exists($filename)) {
            throw new ThreadsException('FILE_NOT_FOUND');
        }

        ++$this->lastId;
        $params['THREAD_ID'] = $this->lastId;
        $extId = $params['ID'];
        $arParams = $params;
        $params = addcslashes(serialize($params), '"');
        $command = "";
        if ($arParams['XDEBUG'] == 'Y') {
            $serverName = $arParams['SERVER_NAME'] ?: $_SERVER['SERVER_NAME'];
            $command .= "export PHP_IDE_CONFIG=\"serverName={$serverName}\" && ";
        }
        $command .= $this->phpPath;
        if ($arParams['XDEBUG'] == 'Y') {
            $xdebugRemotePort = $arParams['XDEBUG_REMOTE_PORT'] ?: 9003;
            $command .= " -dxdebug.remote_enable=1 -dxdebug.remote_mode=req -dxdebug.remote_port={$xdebugRemotePort} -dxdebug.remote_host=127.0.0.1 -dxdebug.remote_connect_back=0 -dxdebug.remote_autostart=1";
        }
        $command .= ' -f ' . $filename . ' -- --params "' . $params . '" ';
        if ($extId) {
            $command .= " --id={$extId}";
        }
        $command .= " &";
        //$command = $this->phpPath.' -f '.$filename.' --params "'.$params.'" > '.dirname($filename).'/log_'.time().$this->lastId;
        $this->handles[$this->lastId] = proc_open($command, $this->descriptorSpec, $pipes);
        $this->streams[$this->lastId] = $pipes[1];
        $this->pipes[$this->lastId] = $pipes;

        return $this->lastId;
    }

    public function iteration()
    {
        if (!count($this->streams)) {
            return false;
        }
        $read = $this->streams;
        stream_select($read, $write = null, $except = null, $this->timeout);
        /* 
            ����� �������� ������ ���� ����� ��� �������� ��������� 
            �� ����� ���� � ������� $read �� �������� ���������
        */
        //$stream = current($read);
        foreach ($read as $stream) {
            $id = array_search($stream, $this->streams);
            $result = stream_get_contents($this->pipes[$id][1]);
            if (feof($stream)) {
                fclose($this->pipes[$id][0]);
                fclose($this->pipes[$id][1]);
                proc_close($this->handles[$id]);
                unset($this->handles[$id]);
                unset($this->streams[$id]);
                unset($this->pipes[$id]);
            }
            $arResult[$id] = $result;
        }

        return $arResult;
    }

    /*
        ��������� ����� ��� ��������� ���������� �� 
        ���������� ��������� ������
    */
    public static function getParams()
    {
        foreach ($_SERVER['argv'] as $key => $argv) {
            if ($argv == '--params' && isset($_SERVER['argv'][$key + 1])) {
                return unserialize($_SERVER['argv'][$key + 1]);
            }
        }
        return false;
    }

}
