<?php

namespace Catalog\Sync\Data\Export;

use Bitrix\Main\Entity\BooleanField;
use Bitrix\Main\Entity\FieldError;
use \Bitrix\Main\Entity\StringField,
    \Bitrix\Main\Entity\IntegerField,
    \Bitrix\Main\Entity\TextField,
    Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Localization\Loc;

class ProfileTable extends DataManager
{
    public static function getTableName()
    {
        return "catalog_sync_export_profile";
    }

    public static function getMap()
    {
        return array(
            new IntegerField("ID", array(
                'primary' => true,
                'autocomplete' => true,
            )),
            new BooleanField("ACTIVE", array()),
            new StringField("NAME", array()),
            new TextField("SETTINGS", array(
                'serialized' => true,
                'validation' => function () {
                    return array(
                        function ($value, $primary, $row, $field) {
                            // value - значение поля
                            // primary - массив с первичным ключом, в данном случае [ID => 1]
                            // row - весь массив данных, переданный в ::add или ::update
                            // field - объект валидируемого поля - Entity\StringField('ISBN', ...)
                            $errorFields = [];
                            if (!$value['URL_DATA_FILE']) {
                                $errorFields[] = 'URL_DATA_FILE';
                            }
                            if (!$value['IBLOCK_ID']) {
                                $errorFields[] = 'IBLOCK_ID';
                            }
                            foreach ($errorFields as $errorField) {
                                return new FieldError(
                                    $field, Loc::getMessage('ERROR_REQUIRED') . ' ' . Loc::getMessage('ERROR_REQUIRED_' . $errorField));
                            }

                            return true;
                        }
                    );
                }
            )),
        );
    }


}