$(function () {
    (function checkStatus() {
        $.ajax({
            url: '/bitrix/admin/catalog_sync_ajax.php',
            dataType: 'json',
        }).done(function (data) {
            BX.onCustomEvent("OnCatalogSyncRefresh", [data]);
            setTimeout(checkStatus, 5000);
        });
    })();
});