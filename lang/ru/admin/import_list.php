<?php
$MESS['rub_f_active'] = 'Активность';
$MESS['POST_YES'] = 'Да';
$MESS['POST_NO'] = 'Нет';
$MESS['POST_ALL'] = 'Все';
$MESS['ACCESS_DENIED'] = 'Доступ запрещен';
$MESS['rub_save_error'] = 'Ошибка сохранения';
$MESS['rub_no_rubric'] = 'Нет данных';
$MESS['rub_del_err'] = 'Ошибка удаления';
$MESS['profile_nav'] = 'Профили';
$MESS['rub_name'] = 'Название';
$MESS['rub_act'] = 'Активность';
$MESS['rub_edit'] = 'Изменить';
$MESS['rub_del'] = 'Удалить';
$MESS['rub_del_conf'] = 'Удалить?';
$MESS['POST_ADD'] = 'Добавить';
$MESS['POST_ADD_TITLE'] = 'Добавить';
$MESS['rub_title'] = 'Список профилей экспорта';
$MESS['RUN_EXPORT'] = 'Запустить';
$MESS['rub_state'] = 'Статус';
$MESS['rub_last_exec'] = 'Время запуска';
$MESS['rub_run'] = 'Запустить';

$MESS ['PROFILE_STATUS_RUN'] = "Запущен";
$MESS ['PROFILE_STATUS_ERROR'] = "Ошибка";
$MESS ['PROFILE_STATUS_FINISH'] = "Завершен";
$MESS ['PROGRESS_TEMPLATE'] = "Выгружено #PROGRESS_VALUE# из #PROGRESS_TOTAL#";
$MESS ['PROFILE_RESULT_SECTIONS'] = "Выгружено разделов";
$MESS ['PROFILE_RESULT_ELEMENTS'] = "Выгружено элементов";
?>