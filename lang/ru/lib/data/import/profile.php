<?php
$MESS['ERROR_REQUIRED'] = "Не заполнено обязательное поле";
$MESS['ERROR_REQUIRED_URL_DATA_FILE'] = "\"Файл для загрузки\"";
$MESS['ERROR_REQUIRED_IBLOCK_ID'] = "\"Информационный блок\"";
$MESS['ERROR_REQUIRED_LID'] = "\"Сайты, к которым будет привязан информационный блок\"";